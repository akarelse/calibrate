<section class="title">
	<h4><?php echo lang('calibrate:'.$this->method); ?></h4>
</section>
<section class="item">
	<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
	<div class="form_inputs">
		<ul>
			<?php if(!empty($items)): foreach ($items as $key => $value) { ?>
			<li class="<?php echo alternator('', 'even'); ?>" id="sensors">
				<label for="slug"><?php echo $value; ?> high value</label>
				<div class="input">
				<?php echo form_input("measured-high-".$value, "", 'class="small-text-field sensor_input" readonly'); ?> <?php echo form_input("real-high".$value, "", 'class="small-text-field"'); ?>
				</div>
				<label for="slug"><?php echo $value; ?> low value</label>
				<div class="input">
				<?php echo form_input("measured-low-".$value, "", 'class="small-text-field sensor_input" readonly'); ?> <?php echo form_input("real-high".$value, "", 'class="small-text-field"'); ?>
				</div>
			</li>
			<?php } endif; ?>
		</ul>
	</div>
	<div class="buttons">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )); ?>
	</div>
	<?php echo form_close(); ?>
</section>