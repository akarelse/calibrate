$(document).ready(function() {
    window.setInterval(updatevalues(), 1000);
    $("form ul #sensors").each(function(index) {
        key = $(this).find(".sensor_input").attr("name").substring(14);
        high = $(this).find("[name=measured-high-" + key + "]");
        low = $(this).find("[name=measured-low-" + key + "]");
        high.val(0);
        low.val(100);
    });
    // high.
    function updatevalues() {
        var newdata = "";
        $.getJSON("index.php/admin/calibrate/show_latest_measument", function(data, status) {
            $("form ul #sensors").each(function(index) {
                key = $(this).find(".sensor_input").attr("name").substring(14);
                high = $(this).find("[name=measured-high-" + key + "]");
                low = $(this).find("[name=measured-low-" + key + "]");
                if (key in data) {
                    if (high.val() < data[key]) {
                        high.val(data[key]);
                    }
                    if (low.val() > data[key]) {
                        low.val(data[key]);
                    }
                } else {
                    $(this).find(".sensor_input").val("N/A");
                }
            });
        });
    }
});