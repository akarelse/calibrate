<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a calibrate module for PyroCMS
 *
 * @author
 * @website
 * @package     PyroCMS
 * @subpackage  Calibrate Module
 */
class Admin extends Admin_Controller
{
    protected $section = 'sensors';
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('calibratesensors_m');
        $this->load->library('form_validation');
        $this->lang->load('calibrate');
        
        $this->item_validation_rules = array(
            array(
                'field' => 'logtime',
                'label' => 'Logtime',
                'rules' => 'required'
            ) ,
            array(
                'field' => 'data',
                'label' => 'Data',
                'rules' => 'trim|max_length[200]'
            )
        );
        $this->template->append_css('module::admin.css');
    }
    
    public function index() {
        $items = $this->calibratesensors_m->get_all();
        $this->template->title($this->module_details['name'])->set('items', $items)->build('admin/items');
    }
    
    public function show_latest_measument() {
        $this->load->model('templog_m');
        $item = $this->templog_m->get_lastest();
        if ($item)
        {
        print $item;
        }
        else
        {
        print "{nothing:0}";
        }
    }
    
    public function view($id = 0) {
        $item = $this->calibratesensors_m->get($id);
        $this->template->title($this->module_details['name'])->set('item', $item)->build('admin/view');
    }
    
    public function create() {
        $this->template->append_js('module::admin.js');
        
        $this->form_validation->set_rules($this->item_validation_rules);
        
        if ($post = $this->input->post()) {
            
            // $names = Events::trigger('get_all_names', $post, 'array');
            $inputa = array_diff_key($post, $this->item_validation_rules);
            unset($inputa['btnAction']);
            $post['data'] = json_encode($inputa);
            
            $time = new DateTime();
            $time->format('Y-m-d');
            $post['logtime'] = $time->format('Y-m-d');
            $this->form_validation->set_data($post);
            if ($this->form_validation->run()) {
                if ($this->calibratesensors_m->create($post)) {
                    $this->session->set_flashdata('success', lang('calibrate.success'));
                    redirect('admin/calibrate');
                } else {
                    $this->session->set_flashdata('error', lang('calibrate.error'));
                    redirect('admin/calibrate/create');
                }
            }
        }

        $items = Events::trigger('get_all_names', array() , 'array');
        $this->template->title($this->module_details['name'], lang('calibrate.new_item'))->set('items', $items[0])->build('admin/form');
    }
    
    public function delete($id = 0) {
        if (isset($_POST['btnAction']) AND is_array($_POST['action_to'])) {
            $this->calibratesensors_m->delete_many($this->input->post('action_to'));
        } elseif (is_numeric($id)) {
            $this->calibratesensors_m->delete($id);
        }
        redirect('admin/calibrate');
    }
}
