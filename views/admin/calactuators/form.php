<section class="title">
	<h4><?php echo lang('calibrate:'.$this->method); ?></h4>
</section>
<section class="item">
	<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
	<?php echo anchor('admin/calibrate/pumps/startpumps', lang('calibrate:calibrate'), 'class="btn orange" target="_blank"'); ?>
	<div class="form_inputs">
		<!-- 		<ul>
			<?php if(!empty($items)): foreach ($items as $key => $value) { ?>
			<li class="<?php echo alternator('', 'even'); ?>" id="sensors">
				<label for="slug"><?php echo $value; ?></label>
				<div class="input"><?php echo form_input($value, "", 'class="width-15 sensor_input" readonly'); ?> <?php echo form_input($value, "", 'class="width-15"'); ?> </div>
			</li>
			<?php } endif; ?>
		</ul> -->
		
	</div>
	<div class="buttons">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )); ?>
	</div>
	<?php echo form_close(); ?>
</section>