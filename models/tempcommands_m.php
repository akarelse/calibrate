<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a calibrate module for PyroCMS
 *
 * @author
 * @website
 * @package 	PyroCMS
 * @subpackage 	Calibrate Module
 */
class Tempcommands_m extends MY_Model
{
    
    public function __construct() {
        parent::__construct();
        $this->_table = 'tempcommands';
    }
    
    public function create_command($data) {
        $time = new DateTime();
        $data['logtime'] = $time->format('Y-m-d');
        return $this->db->insert($this->_table, $data);
    }
    
    public function clear_old_commands() {
        $time = new DateTime();
        $time->sub(new DateInterval('PT10S'));
        $this->db->where('logtime <', $time->format('Y-m-d H:i:s'));
        $this->db->delete($this->_table);
    }
    
    public function clear_all_commands() {
        $this->db->delete($this->_table);
    }
    
    public function get_active_commands() {
        $time = new DateTime();
        $time->sub(new DateInterval('PT10S'));
        $this->db->where('logtime >', $time->format('Y-m-d H:i:s'))->get($this->_table)->result();
    }
}
