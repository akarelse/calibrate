<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Module_Calibrate extends Module
{
    public $version = '2.1';
    
    public function info() {
        return array(
            'name' => array(
                'en' => 'Calibrate',
                'nl' => 'Kalibreer'
            ) ,
            'description' => array(
                'en' => 'This is a PyroCMS module calibrate.',
                'nl' => 'Dit is de kalibreer module voor pyrocms'
            ) ,
            'frontend' => TRUE,
            'backend' => TRUE,
            'menu' => 'content',
            'sections' => array(
                'sensors' => array(
                    'name' => 'calibrate:sensors',
                    'uri' => 'admin/calibrate',
                    'shortcuts' => array(
                        'create' => array(
                            'name' => 'calibrate:create',
                            'uri' => 'admin/calibrate/create',
                            'class' => 'add'
                        )
                    )
                ) ,
                'calactuators' => array(
                    'name' => 'calibrate:actuators',
                    'uri' => 'admin/calibrate/calactuators',
                    'shortcuts' => array(
                        'create' => array(
                            'name' => 'calibrate:create',
                            'uri' => 'admin/calibrate/calactuators/create',
                            'class' => 'add'
                        )
                    )
                )
            )
        );
    }
    
    public function install() {
        $this->dbforge->drop_table('calibrate');
        $this->dbforge->drop_table('templog');
        
        $calibratesensors = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ) ,
            'logtime' => array(
                'type' => 'DATETIME',
                'null' => false,
                'default' => '0001-01-01 01:01:01'
            ) ,
            'data' => array(
                'type' => 'VARCHAR',
                'constraint' => '200',
                'default' => '{"item":"empty"}'
            )
        );
        
        $calibrateactuators = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ) ,
            'logtime' => array(
                'type' => 'DATETIME',
                'null' => false,
                'default' => '0001-01-01 01:01:01'
            ) ,
            'data' => array(
                'type' => 'VARCHAR',
                'constraint' => '200',
                'default' => '{"item":"empty"}'
            )
        );
        
        $templog = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ) ,
            'slug' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'default' => 'default'
            ) ,
            'data' => array(
                'type' => 'VARCHAR',
                'constraint' => '200',
                'default' => '{"item":"empty"}'
            )
        );
        
        $tempcommand = array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ) ,
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'default' => 'default'
            ) ,
            'parameter' => array(
                'type' => 'INT',
                'constraint' => '1',
                'default' => 0
            ) ,
            'logtime' => array(
                'type' => 'DATETIME',
                'null' => false,
                'default' => '0001-01-01 01:01:01'
            )
        );
        
        $this->db->trans_start();
        
        $this->dbforge->add_field($calibratesensors);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('calibratesensors');
        
        $this->dbforge->add_field($calibrateactuators);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('calibrateactuators');
        
        $this->dbforge->add_field($templog);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('templog');
        
        $this->dbforge->add_field($tempcommand);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('tempcommand');
        
        $this->db->trans_complete();
        
        return $this->db->trans_status();
    }
    
    public function uninstall() {
        $this->dbforge->drop_table('calibratesensors');
        $this->dbforge->drop_table('calibrateactuators');
        $this->dbforge->drop_table('templog');
        $this->dbforge->drop_table('tempcommand');
        
        return TRUE;
    }
    
    public function upgrade($old_version) {
        return TRUE;
    }
    
    public function help() {
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }
}