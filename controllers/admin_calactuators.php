<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a calibrate module for PyroCMS
 *
 * @author
 * @website
 * @package 	PyroCMS
 * @subpackage
 */
class Admin_calactuators extends Admin_Controller
{
    
    protected $section = 'calactuators';
    
    public function __construct() {
        parent::__construct();
        $this->load->model('calibrateactuators_m');
        $this->load->library('form_validation');
        $this->lang->load('calibrate');
        
        $this->item_validation_rules = array(
            array(
                'field' => 'logtime',
                'label' => 'Logtime',
                'rules' => 'required'
            ) ,
            array(
                'field' => 'data',
                'label' => 'Data',
                'rules' => 'trim|max_length[200]'
            )
        );
    }
    
    public function index() {
        $data['items'] = $this->calibrateactuators_m->get_all();
        $this->template->title($this->module_details['name'])->build('admin/calactuators/items', $data);
    }
    
    public function run_actuator($id) {
        $this->load->model('tempcommands_m');
    }
    
    public function view($id = 0) {
        $item = $this->calibrateactuators_m->get($id);
        $this->template->title($this->module_details['name'])->set('item', $item)->build('admin/calactuators/items');
    }
    
    public function create() {
        $this->template->append_js('module::admin.js');
        $data['actuators'] = Events::trigger('all_actuators', array() , 'array');
        $this->form_validation->set_rules($this->item_validation_rules);
        
        if ($post = $this->input->post()) {
            
            // $names = Events::trigger('get_all_names', $post, 'array');
            $inputa = array_diff_key($post, $this->item_validation_rules);
            unset($inputa['btnAction']);
            $post['data'] = json_encode($inputa);
            
            $time = new DateTime();
            $time->format('Y-m-d');
            $post['logtime'] = $time->format('Y-m-d');
            $this->form_validation->set_data($post);
            if ($this->form_validation->run()) {
                if ($this->calibrateactuators_m->create($post)) {
                    $this->session->set_flashdata('success', lang('calibrate.success'));
                    redirect('admin/calibrate');
                } else {
                    $this->session->set_flashdata('error', lang('calibrate.error'));
                    redirect('admin/calibrate/calactuators/create');
                }
            }
        }
        $items = Events::trigger('get_names_by_name', array(
            'name' => 'blaat'
        ) , 'array');
        
        $this->template->title($this->module_details['name'], lang('calibrate.new_item'))->set('items', $items[0])->build('admin/calactuators/form');
    }
    
    public function delete($id = 0) {
        if (isset($_POST['btnAction']) AND is_array($_POST['action_to'])) {
            $this->calibrateactuators_m->delete_many($this->input->post('action_to'));
        } elseif (is_numeric($id)) {
            $this->calibrateactuators_m->delete($id);
        }
        redirect('admin/actuators/calibrate');
    }
}
