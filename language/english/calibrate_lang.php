<?php
//messages
$lang['calibrate:success']			=	'It worked';
$lang['calibrate:error']			=	'It didn\'t work';
$lang['calibrate:no_items']			=	'No Items';

//page titles
$lang['calibrate:create']			=	'Create Item';

//labels
$lang['calibrate:name']				=	'Name';
$lang['calibrate:slug']				=	'Slug';
$lang['calibrate:manage']			=	'Manage';
$lang['calibrate:item_list']		=	'Item List';
$lang['calibrate:view']				=	'View';
$lang['calibrate:edit']				=	'Edit';
$lang['calibrate:delete']			=	'Delete';

$lang['calibrate:sensors']			=	'Sensors';
$lang['calibrate:actuators']		=	'Actuators';


//buttons
$lang['calibrate:custom_button']	=	'Custom Button';
$lang['calibrate:items']			=	'Items';
$lang['calibrate:calibrate']		=	'Calibrate';
$lang['calibrate:start']			=	'Start';
?>