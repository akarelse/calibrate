<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a calibrate module for PyroCMS
 *
 * @author
 * @website
 * @package 	PyroCMS
 * @subpackage 	calibrate Module
 */
class Calibrateactuators_m extends MY_Model
{
    
    public function __construct() {
        parent::__construct();
        $this->_table = 'calibrateactuators';
    }
}
