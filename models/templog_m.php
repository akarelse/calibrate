<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a calibrate module for PyroCMS
 *
 * @author
 * @website
 * @package     PyroCMS
 * @subpackage  Calibrate Module
 */
class Templog_m extends MY_Model
{
    
    public function __construct() {
        parent::__construct();
        $this->_table = 'templog';
    }
    
    public function log_this($input) {
        $to_insert = array(
            'data' => $input['data']
        );
        return $this->db->insert($this->_table, $to_insert);
    }
    
    public function get_lastest() {
        $results = $this->db->order_by("id", "desc")->get($this->_table, 1, 0)->result();
        unset($results->id);
        
        if ($results) {
            return $results[0]->data;
        }
        return "";
    }

    public function get_all_names_latest()
    {
        $results = $this->db->get($this->_table)->result();
        $tempar = array();
        foreach ($results as $value) {
            $dataar = (array)json_decode($value->data);
            $tempar = array_merge(array_keys($dataar) , $tempar);
        }
        $tempar = (object)array_unique($tempar);
        // print_r($tempar);
        return $tempar;
    }
    
    public function _check_slug($slug) {
        $slug = strtolower($slug);
        $slug = preg_replace('/\s+/', '-', $slug);
        return $slug;
    }
}
