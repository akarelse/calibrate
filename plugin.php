<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a calibrate module for PyroCMS
 *
 * @author
 * @website
 * @package 	PyroCMS
 * @subpackage 	Calibrate Module
 */
class Plugin_Calibrate extends Plugin
{
}

/* End of file plugin.php */
