<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Calibrate Events Class
 *
 * @package        PyroCMS
 * @subpackage    Calibrate Module
 * @category        events
 * @author
 * @website
 */
class Events_Calibrate
{
    
    protected $ci;
    
    /**
     * Constructor
     * @return void
     */
    public function __construct() {
        $this->ci = & get_instance();
        Events::register('calibrate_data_log', array(
            $this,
            'calibrate_log'
        ));
        Events::register('calibrate_commands', array(
            $this,
            'calibrate_command'
        ));
        Events::register('get_all_names', array(
            $this,
            'get_all_names'
        ));
    }
    
    /**
     * events run modul to be removed
     * @return void
     */
    public function calibrate_log($data) {
        $this->ci->load->model('calibrate/templog_m');
        $this->ci->templog_m->log_this($data);
    }
    
    public function calibrate_command() {
        $this->ci->load->model('calibrate/calibratesensors_m');
        $this->ci->calibratesensors_m->limit(5)->get_all();
    }

    public function get_all_names()
    {
       $this->ci->load->model('calibrate/templog_m');
       return $this->ci->templog_m->get_all_names_latest();        
    }
}

/* End of file events.php */
