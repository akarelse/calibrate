<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a calibrate module for PyroCMS
 *
 * @author
 * @website
 * @package 	PyroCMS
 * @subpackage 	calibrate Module
 */
class Calibratesensors_m extends MY_Model
{
    
    public function __construct() {
        parent::__construct();
        
        /**
         * If the calibrate module's table was named "calibrates"
         * then MY_Model would find it automatically. Since
         * I named it "calibrate" then we just set the name here.
         */
        $this->_table = 'calibratesensors';
    }
    
    //create a new item
    public function create($input) {
        $to_insert = array(
            'logtime' => $input['logtime'],
            'data' => $input['data']
        );
        
        return $this->db->insert($this->_table, $to_insert);
    }
}
